#include "parser.h"

#include <iostream>
#include <fstream>
#include "ctre.hpp"

/*** File: info
[General]
Name=JVC HA-S30BT
Class=0x240404
SupportedTechnologies=BR/EDR;
Trusted=true
Blocked=false
Services=00001108-0000-1000-8000-00805f9b34fb;0000110b-0000-1000-8000-00805f9b34fb;0000110c-0000-1000-8000-00805f9b34fb;0000110e-0000-1000-8000-00805f9b34fb;0000111e-0000-1000-8000-00805f9b34fb;

[LinkKey]
Key=9967BB835B7BAEAF9B114D52D1EE9F8D
Type=4
PINLength=0
*/

const std::filesystem::path bluetooth{"../docs/bluetooth"};

bool is_mac(std::string_view dir_entry);
std::string get_mac(std::string_view line);
bool exist_info(std::filesystem::path dir);
void modify_key(std::string_view key,std::filesystem::path origin, std::filesystem::path modified);

bool is_mac(std::string_view dir_entry)
{
    static constexpr auto pattern = ctll::fixed_string{"([0-9A-Za-z]{2}[:]){5}([0-9A-Za-z]{2})"};
    return bool(ctre::match<pattern>(dir_entry));
}

bool exist_info(std::filesystem::path dir)
{
    return std::filesystem::exists(dir);
}

std::string get_name(std::string_view line)
{
    static constexpr auto pattern = ctll::fixed_string{"Name=([0-9A-Za-z]+)"};
    auto m = ctre::match<pattern>(line);
    if (m == true)
    {
        return m.get<1>().to_string();
    }

    return "";
}

std::string get_key(std::string_view line)
{
    static constexpr auto pattern = ctll::fixed_string{"Key=([0-9A-Za-z ]+)"};
    auto m = ctre::match<pattern>(line);
    if (m == true)
    {
        return m.get<1>().to_string();
    }

    return "";
}

std::string get_mac(std::string_view line)
{
    static constexpr auto pattern = ctll::fixed_string{"MAC=([0-9A-Za-z]{2}[:]){5}([0-9A-Za-z]{2})"};
    auto m = ctre::match<pattern>(line);
    if (m == true)
    {
        std::cout << "MAC:" << m.get<1>().to_string() << std::endl;
        return m.get<1>().to_string();
    }
    else
    {
        std::cout << "NO MAC:" << line << std::endl;
    }

    return "";
}

std::vector<adapter_t> list_host_adapters()
{
    std::vector<adapter_t> host_adapters;
    std::string_view folder_content{};

    for (auto const &dir_entry : std::filesystem::directory_iterator{bluetooth})
    {
        std::string_view folder_content{dir_entry.path().string()};
        folder_content.remove_prefix(bluetooth.string().length() + 1);
        if (is_mac(folder_content) == true)
        {
            adapter_t mac{};
            mac.MAC = folder_content;
            host_adapters.push_back(mac);
        }
    }
    return host_adapters;
}

std::vector<devices_t> list_devices(const adapter_t &host_mac)
{
    std::vector<devices_t> client_adapters;
    std::filesystem::path client_path = bluetooth;
    client_path /= host_mac.MAC;

    for (auto const &dir_entry : std::filesystem::directory_iterator{client_path})
    {
        std::string_view folder_content{dir_entry.path().string()};
        folder_content.remove_prefix(client_path.string().length() + 1);
        if (is_mac(folder_content) == true)
        {
            std::filesystem::path info_path = dir_entry.path();
            info_path /= "info";
            std::cout << "info_path: " << info_path.string() << std::endl;
            devices_t device = get_info(info_path);
            device.MAC = folder_content;
            client_adapters.push_back(device);
        }
    }

    return client_adapters;
}

devices_t get_info(std::filesystem::path dir)
{
    devices_t device{};
    if (exist_info(dir) == true)
    {
        std::ifstream info{dir};
        if (info.fail() == true)
        {
            std::cout << "Unable to open device info" << std::endl;
        }
        else
        {
            std::string next_line;
            while (info >> next_line)
            {
                if (std::string name = get_name(next_line); name.length() > 0)
                {
                    device.name = name;

                }
                else if (std::string key = get_key(next_line); key.length() > 0)
                {
                    device.key = key;
                }
                else if (std::string mac = get_mac(next_line); mac.length() > 0)
                {
                    device.MAC = mac;
                }
                
            }
        }
    }
    else
    {
        std::cout << "File info doesn't exists" << std::endl;
    }
    return device;
}

int export_info(devices_t selected_device)
{
    std::string filename = selected_device.name + ".unifyBT";
    std::ofstream export_file(filename);
    if (export_file.fail())
    {
        std::cout << "Unable to open export file" << std::endl;
        return EXIT_FAILURE;
    }
    export_file << "Name=" << selected_device.name << "\r\n";
    export_file << "MAC=" << selected_device.MAC << "\r\n";
    export_file << "Key=" << selected_device.key << "\r\n";

    export_file.close();
    std::cout << "File exported sucesfully" << std::endl;
    return EXIT_SUCCESS;
}

int import_info(std::filesystem::path file_path)
{
    std::ifstream import_file{file_path};
    if (import_file.fail() == true)
    {
        std::cerr << "Unable to open device info" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::string next_line;
        devices_t import_device = get_info(file_path);

        if (import_device.key.length() == 0 || import_device.MAC.length() == 0)
        {
            std::cerr << "Wrong file format. Abort" << std::endl;
            return EXIT_FAILURE;
        }

        std::filesystem::path backup_info{bluetooth};
        backup_info /= import_device.MAC;
        backup_info /= "info.backup";
 
        std::filesystem::path new_info{bluetooth};
        new_info /= import_device.MAC;
        new_info /= "info";

        if (std::filesystem::copy_file(file_path, backup_info))
        {
            modify_key(import_device.key,backup_info,new_info);
        }
        else
        {
            std::cerr << "Unable to make a backup of info. Abort" << std::endl;
        }

        return EXIT_SUCCESS;
    }
}

void modify_key(std::string_view key,std::filesystem::path origin, std::filesystem::path modified)
{
    std::ofstream dest(modified);
    std::ifstream src{origin};

    std::string next_line;
    while (src >> next_line)
    {
        if (get_key(next_line).length() > 0)
        {
            dest << "Key=" << key << "\r\n";
        }
        else
        {
            dest << next_line << std::endl;
        }
    }


}