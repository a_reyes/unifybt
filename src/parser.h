#ifndef PARSER_H
#define PARSER_H

#include <string>
#include <vector>
#include <filesystem>

struct adapter_t
{
    std::string MAC{};
};

struct devices_t
{
    std::string MAC{};
    std::string name{};
    std::string key{};
};

std::vector<adapter_t> list_host_adapters();
std::vector<devices_t> list_devices(const adapter_t& host_mac);
std::string get_name(std::string_view line);
std::string get_key(std::string_view line);
devices_t get_info(std::filesystem::path dir);

int export_info(devices_t device);
int import_info(std::filesystem::path file_path);

#endif