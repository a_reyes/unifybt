#include <iostream>
#include <vector>
#include <fstream>
#include "parser.h"

std::string_view separator{"===================================================\r\n"};

int main()
{
    std::vector<adapter_t> host_adapters = list_host_adapters();
    if (host_adapters.size() > 0)
    {
        std::cout << separator;
        std::cout << "Select the adapter: \r\n";
        int host_id = 0;
        for (adapter_t adapter : host_adapters)
        {
            std::cout << "[" << (host_id) << "] " << adapter.MAC << "\r\n";
            host_id++;
        }
        std::cout << std::endl;
        int selected_host_id = host_adapters.size();
        while (selected_host_id >= host_adapters.size())
        {
            std::cin >> selected_host_id;
        }

        std::cout << "Selected " << host_adapters.at(selected_host_id).MAC << std::endl;
        std::cout << separator;
        std::vector<devices_t> devices_adapters = list_devices(host_adapters.at(selected_host_id));
        std::cout << "Select the client: \r\n";
        int device_id = 0;
        for (devices_t device : devices_adapters)
        {
            std::cout << "[" << (device_id) << "] " << device.MAC << " " << device.name << " "
                      << "\r\n";
            device_id++;
        }
        std::cout << std::endl;
        int selected_device_id = devices_adapters.size();
        while (selected_device_id >= devices_adapters.size())
        {
            std::cin >> selected_device_id;
        }
        devices_t selected_device = devices_adapters.at(selected_device_id);
        std::cout << "Selected " << selected_device.name << std::endl;
        while (true)
        {
            std::cout << "Choose action: [e] Export key   [i] Import key   [m] Modify key   [x] Exit" << std::endl;
            std::string action;
            std::cin >> action;
            switch (action.at(0))
            {
            case 'e':
            {
                if (export_info(selected_device) == EXIT_SUCCESS)
                {
                    return EXIT_SUCCESS;
                }
                break;
            }
            case 'i':
            {
                std::cout << "Path to file:" << std::endl;
                std::string import_path;
                std::cin >> import_path;

                if (std::filesystem::path file_path{import_path}; file_path.has_filename())
                {
                    import_info(file_path);
                }
                else
                {
                    std::cerr << "File not found: " << import_path << std::endl;
                    return EXIT_FAILURE;
                }
                break;
            }
            case 'm':
            { /* code */
                break;
            }
            case 'x':
            {
                return EXIT_SUCCESS;
                break;
            }
            default:
                break;
            }
        }
    }
    else
    {
        std::cout << "Not adapter found" << std::endl;
    }

    return EXIT_SUCCESS;
}